TEMPLATE = app

QT += qml quick

RESOURCES += \
    resources/app.qrc
RC_FILE = resources/rc/App.rc

INCLUDEPATH += include

SOURCES += src/main.cpp \
    src/MainAppController.cpp \
    src/HomeAdapter.cpp \
    src/WhiteBoardAdapter.cpp \
    src/ConnectionManager.cpp

HEADERS += \
    include/IMainAppController.h \
    include/MainAppController.h \
    include/AppConst.h \
    include/HomeAdapter.h \
    include/IHomeAdapter.h \
    include/IWhiteBoardAdapter.h \
    include/WhiteBoardAdapter.h \
    include/ConnectionManager.h

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

DISTFILES += \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/AndroidManifest.xml \
    android/res/values/libs.xml \
    android/build.gradle \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew \
    android/gradlew.bat

ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android
