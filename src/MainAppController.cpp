/*********************************************
*****                              ***********
***     Author: Safeer Chonengal    **********
***     Application: AirBoardApp    **********
***     Date: 03/04/2016            **********
*****                           **************
***********************************************/

#include <MainAppController.h>
#include <AppConst.h>

#include <QQmlComponent>
#include <QQmlContext>
#include <QQuickItem>
#include <QThread>
#include <assert.h>
#include <QSysInfo>
#include <QScreen>
#include <QGuiApplication>
#include <HomeAdapter.h>

namespace AirBoardApp
{

    MainAppController::MainAppController(QObject* parent)
        :QObject(parent)
        ,_currentState(UNDEFINED)
    {

    }

    MainAppController::~MainAppController(void)
    {

    }

    void
    MainAppController::initialize(QQmlApplicationEngine* engine)
    {
        _engine = engine;

        // Pass Airboard theme color into QML Context
        _engine->rootContext()->setContextProperty("airboardColor", QVariant(AIRBOARD_THEME_COLOR));

        QScreen* screen = QGuiApplication::screens()[0];
        Qt::ScreenOrientation orientation = screen->nativeOrientation();

        // Get device
        QString productType = QSysInfo::productType();
        QString device = "unknown";
        if(productType.contains("windows"))
        {
            device = "windows-desktop";
        }
        else if(productType.contains("android"))
        {
            if(orientation == Qt::PortraitOrientation)
            {
                device = "android-phone";
            }
            else
            {
                device = "android-tablet";
            }
        }
        else if(productType.contains("winphone"))
        {
            device = "windows-phone";
        }

        _engine->rootContext()->setContextProperty("device", QVariant(device));

        qDebug() << "Device: " << device << endl;
        qDebug() << "Native orientation: " << orientation << endl;

        // Add components into the loader list
        _loaderList.push_back("LoadingScreen");
        _loaderList.push_back("HomeScreen");

        // Keep the iterator in the beginning of the list
        _loaderItr = _loaderList.begin();

        // Connect with timer
        bool connected = false;
        connected = connect(&_mainTimer,
                            SIGNAL(timeout()),
                            this,
                            SLOT(_update()));

        // Initialize the Timer and run
        _mainTimer.setInterval(50);
        _mainTimer.setSingleShot(false);
        _mainTimer.start();
    }

    void
    MainAppController::_update()
    {
        // Try Applying the current state and return next state
        _currentState = _applyState(_currentState);

    }

    bool
    MainAppController::_loadComponent()
    {
        if(_loaderItr == _loaderList.end())
        {
            // We are done with loading
            return true;
        }

        QString componentName = *_loaderItr;

        if( componentName.contains("LoadingScreen"))
        {
            // This is the root component
            _engine->load(QUrl(QStringLiteral("qrc:///AppLoading.qml")));
            _loaderItr++;
        }
        else if( componentName.contains("HomeScreen"))
        {
            _loadHomeComponent();
            _loaderItr++;
        }
        else
        {
            qCritical() << "_loadComponent(): Can't identify the component " << componentName << endl;
            assert(false);
        }

        // Still to load components
        return false;
    }

    IMainAppController::AppState
    MainAppController::_applyState(const AppState& state)
    {
        // Apply state and return next state to go
        if(state == UNDEFINED)
        {
            return APP_LOADING;
        }
        else if(state == APP_LOADING)
        {
            if(_loadComponent())
            {
                // If loading is complete we launch App Home
                return LAUNCH_HOME;
            }
            else
            {
                // Yet more components to load,
                // hence return App Loading itself
                return APP_LOADING;
            }
        }
        else if(state == LAUNCH_HOME)
        {
            _homeAdapter->launchWindow();
            return APP_LOADING_COMPLETE;
        }
        else if(state == APP_LOADING_COMPLETE)
        {
            return APP_LOADING_COMPLETE;
        }
    }

    void
    MainAppController::_loadHomeComponent()
    {

        QObject* rootObj = _engine->rootObjects()[0];
        rootObj = rootObj->findChild<QObject*>("MainContainer");
        QQuickItem* rootItem = dynamic_cast<QQuickItem*>(rootObj);

        QQmlComponent homeComponent(_engine, QUrl(QStringLiteral("qrc:///MainContainer.qml")));
        QObject* homeObj = homeComponent.create();
        QQuickItem* homeItem = dynamic_cast<QQuickItem*>(homeObj);

        _homeAdapter = QSharedPointer<IHomeAdapter>(new HomeAdapter(this));

        if(homeItem && rootItem)
        {
            rootItem->setProperty("loadingDone", QVariant(true));
            homeItem->setParentItem(rootItem);
            _homeAdapter->setGUIObject(homeObj);
            _homeAdapter->initialize(_engine);
        }
        else
        {
            qCritical() << "_loadComponent(): Error. Failed to load home component." << endl;
            assert(false);
            QCoreApplication::quit();
        }
    }
}
