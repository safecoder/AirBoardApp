/*********************************************
*****                              ***********
***     Author: Safeer Chonengal    **********
***     Application: AirBoardApp    **********
***     Date: 03/04/2016            **********
*****                           **************
***********************************************/

#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <MainAppController.h>
#include <AppConst.h>

#include <QWindow>
#include <QDebug>
#include <QDateTime>
#include <QTextStream>
#include <QDir>

/// \brief  This is the default Qt Message Handler
///         We want to call this along with custom messageHandler that we define
QtMessageHandler defaultMessageHandler;

/// \brief  Application start time. We keep this in the log file name also.
QDateTime applicationStartTime;

/// \brief  Custom message handler for printing logs wherever we like
void messageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg);

// Application entry point
int main(int argc, char *argv[])
{
    // Get default message handler
    defaultMessageHandler = qInstallMessageHandler(0);

    // Register custom message handling method for generating logs
    qInstallMessageHandler(messageHandler);

    // Get Application start time
    applicationStartTime = QDateTime::currentDateTime();
    qDebug() << APP_NAME<< "::main() Applicaion started at " << applicationStartTime.toString("dd MMM yyyy hh:mm:ss:zzzAP");

    QGuiApplication app(argc, argv);
    QQmlApplicationEngine engine;

    QSharedPointer<AirBoardApp::IMainAppController> mainController
            = QSharedPointer<AirBoardApp::IMainAppController>(new AirBoardApp::MainAppController(NULL));
    mainController->initialize(&engine);

    app.setApplicationName(APP_NAME);
    app.setApplicationDisplayName(APP_NAME);

    qDebug() << APP_NAME<< "::main() Done launching." << endl;
    return app.exec();
}

// Definition of custom message handler
void messageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    // Logging time
    QDateTime dateTime = QDateTime::currentDateTime();
    QString messageStr = dateTime.toString("<br>dd MMM yyyy hh:mm:ss:zzz ");

    // Add log type and HTML tags for printing
    switch (type) {
    case QtDebugMsg:
        messageStr.append("[DEBUG] ");
        messageStr.append(msg);
        break;
    case QtWarningMsg:
        messageStr.append("[WARNING] ");
        messageStr.append(msg);
        break;
    case QtCriticalMsg:
        messageStr.append("<font color='red'>[CRITICAL] ");
        messageStr.append(msg);
        messageStr.append("</font>");
        break;
    case QtFatalMsg:
        messageStr.append("<font color='red'>[FATAL] ");
        messageStr.append(msg);
        messageStr.append("</font>");
        break;
    }

#ifdef WIN32
    QString dataFolder = "";
    char* envvarvalue = getenv("APPDATA");
    if(envvarvalue)
    {
        dataFolder = envvarvalue;
        QString folderName = dateTime.toString("dd_MMM_yyyy");
        dataFolder += "\\AirBoard\\Logs\\" + folderName;
        dataFolder = QDir::toNativeSeparators(dataFolder);
        QDir dir(dataFolder);

        // Check the existance of the target folder path for creating log files
        // If not, create the directories
        if (!dir.exists()) {
            dir.mkpath(".");
        }
        QString logFileName =   dataFolder + QDir::separator() +
                                    QString(AB_LOG_OUTPUT_FILE_NAME_PREFIX) + QString("_");
        logFileName += applicationStartTime.toString("dd-MM-yyyy_hh_mm_ss_zzzAP") + ".html";

        logFileName = QDir::toNativeSeparators(logFileName);
        QFile outputFile(logFileName);
        bool fileOpenForWriting = outputFile.open(QIODevice::WriteOnly | QIODevice::Append);
        if(fileOpenForWriting)
        {
            QTextStream stream(&outputFile);
            stream << messageStr << endl << flush;
            outputFile.close();
        }
    }
#endif

    defaultMessageHandler(type, context, msg);
}
