/*********************************************
*****                              ***********
***     Author: Safeer Chonengal    **********
***     Application: AirBoard    **********
***     Date: 2016_04_11            **********
*****                           **************
***********************************************/

#include <WhiteBoardAdapter.h>
#include <ConnectionManager.h>
#include <QQuickItem>
#include <assert.h>

namespace AirBoardApp
{
    WhiteBoardAdapter::WhiteBoardAdapter(QObject* parent)
        :QObject(parent)
        ,_whiteBoardGui(NULL)
        ,_uuid("")
    {

    }

    WhiteBoardAdapter::~WhiteBoardAdapter(void)
    {

    }

    void
    WhiteBoardAdapter::initialize(QQmlApplicationEngine* engine, QObject* homeObj)
    {
        _engine = engine;

        if(!homeObj)
        {
            qCritical() << "initialize(): Error. Home GUI object passed is NULL." << endl;
            assert(false);
            return;
        }

        // Create Gui component
        QQmlComponent guiComponent(_engine, QUrl(QStringLiteral("qrc:///WhiteBoard.qml")));
        _whiteBoardGui = guiComponent.create();

        if(!_whiteBoardGui)
        {
            qCritical() << "initialize(): Error. White board gui component can't be created." << endl;
            assert(false);
            return;
        }

        // Set parent
        QQuickItem* parentItem = dynamic_cast<QQuickItem*>(homeObj);
        QQuickItem* whiteBoardItem = dynamic_cast<QQuickItem*>(_whiteBoardGui);
        whiteBoardItem->setParentItem(parentItem);

        _connectSignals();
    }

    void
    WhiteBoardAdapter::_handleMessage(QString msgString)
    {
        if(!msgString.isNull() && !msgString.isEmpty())
        {
            _connectionManager->broadcastMessage(msgString);
        }
        else
        {
            qWarning() << "WhiteBoardAdapter::_handleMessage() - Null String passed" << endl;
        }
    }

    void
    WhiteBoardAdapter::_connectSignals()
    {
        bool connected=false;
        connected = connect(_whiteBoardGui,
                            SIGNAL(sendMsg(QString)),
                            this,
                            SLOT(_handleMessage(QString)));

        assert(connected);

        connected = connect(_whiteBoardGui,
                            SIGNAL(broadcastFlagChanged(bool)),
                            this,
                            SLOT(_handleBroadcastFlagChanged(bool)));

        assert(connected);

        connected = connect(_whiteBoardGui,
                            SIGNAL(subscriptionFlagChanged(bool)),
                            this,
                            SLOT(_handleSubscriptionFlagChanged(bool)));

        assert(connected);
    }

    void
    WhiteBoardAdapter::_handleBroadcastFlagChanged(bool flag)
    {
        Q_UNUSED(flag)
    }

    void
    WhiteBoardAdapter::_handleSubscriptionFlagChanged(bool flag)
    {
        if(flag)
            _connectionManager->subscribeRemoteWBoard(_whiteBoardGui);
        else
            // TODO: Unsubscribe here
            return;
    }

    void
    WhiteBoardAdapter::setUuid(const QString& id)
    {
        _uuid = id;

        if(_whiteBoardGui)
        {
            _whiteBoardGui->setProperty("uuid", QVariant(id));
        }
    }

    const QString
    WhiteBoardAdapter::getUuid() const
    {
        return _uuid;
    }

    void
    WhiteBoardAdapter::setName(const QString& name)
    {
        _name = name;

        if(_whiteBoardGui)
        {
            _whiteBoardGui->setProperty("name", QVariant(name));
        }
    }

    const QString
    WhiteBoardAdapter::getName() const
    {
        return _name;
    }

    void
    WhiteBoardAdapter::setConnectionManager(ConnectionManager* connectionManager)
    {
        assert(connectionManager);
        _connectionManager = connectionManager;
    }

}
