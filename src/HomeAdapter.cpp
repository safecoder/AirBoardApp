/*********************************************
*****                              ***********
***     Author: Safeer Chonengal    **********
***     Application: AirBoard    **********
***     Date: 2016_04_11            **********
*****                           **************
***********************************************/

#include <HomeAdapter.h>
#include <WhiteBoardAdapter.h>

#include <QDebug>
#include <QUuid>
#include <assert.h>
#include <QUuid>

#include <QJsonDocument>
#include <QJsonObject>

namespace AirBoardApp
{
    HomeAdapter::HomeAdapter(QObject* parent)
        :QObject(parent)
        ,_homeQmlComponent(NULL)
    {
    }

    HomeAdapter::~HomeAdapter(void)
    {
    }

    void
    HomeAdapter::initialize(QQmlApplicationEngine* engine)
    {
        _engine = engine;

        if(!_homeQmlComponent)
        {
            // At this point we must get a valid GUI Object for home
            qCritical() << "initialize(): home Gui Component is null. Can't initialize." << endl;
            return;
        }

        // Generate Uuid for this Window
        QString Uuid = QUuid::createUuid().toString();
        setUuid(Uuid);

        // Initialize Connection Manager
        _connectionManager = QSharedPointer<ConnectionManager>(new ConnectionManager());
        _connectionManager->initialize();

        // Connect GUI signals
        bool connected;
        connected = connect(_homeQmlComponent,
                            SIGNAL(createAirBoard(QString)),
                            this,
                            SLOT(_handleCreateAirBoardRequest(QString)));
        assert(connected);
    }

    void
    HomeAdapter::_handleCreateAirBoardRequest(const QString& inputJson)
    {
        QSharedPointer<IWhiteBoardAdapter> newWhiteBoard =
                QSharedPointer<IWhiteBoardAdapter>(new WhiteBoardAdapter());
        if(!newWhiteBoard.isNull())
        {
            newWhiteBoard->initialize(_engine, _homeQmlComponent);

            // Generate Uuid
            QString newId = QUuid::createUuid().toString();
            newWhiteBoard->setUuid(newId);

            // Set connection manager
            newWhiteBoard->setConnectionManager(_connectionManager.data());

            // Get the name and email id from inputJson
            // This needs to be passed down to new Whiteboard object
            QJsonDocument doc = QJsonDocument::fromJson(inputJson.toUtf8());
            if(!doc.isNull() && doc.isObject())
            {
                QJsonObject jsonObj = doc.object();
                QString whiteBoardName = jsonObj.value("name").toString();
                newWhiteBoard->setName(whiteBoardName);
            }

            // launch this AirBoard
            launchWindow(newId);

            _whiteBoardList.insert(newId, newWhiteBoard);
        }
    }

    void
    HomeAdapter::setGUIObject(QObject* qObj)
    {
        if(!qObj)
        {
            qCritical() << "setGUIObject(): Setting null objects for GUI Item" << endl;
            assert(false);
            return;
        }
        _homeQmlComponent = qObj;
    }

    QObject*
    HomeAdapter::getGUIObject() const
    {
        return _homeQmlComponent;
    }

    void
    HomeAdapter::setUuid(const QString& id)
    {
        _uuid = id;

        if(_homeQmlComponent)
        {
            _homeQmlComponent->setProperty("uuid", QVariant(id));
        }
    }

    const QString
    HomeAdapter::getUuid() const
    {
        return _uuid;
    }

    void
    HomeAdapter::launchWindow(const QString& uuid)
    {
        if(!_homeQmlComponent)
        {
            qCritical() << "launchWindow(): Home component is NULL" << endl;
            return;
        }

        QString uuidToLoad = _uuid;
        if(uuid != "")
        {
            uuidToLoad = uuid;
        }
        _homeQmlComponent->setProperty("currentWindowUuid", uuidToLoad);
    }
}
