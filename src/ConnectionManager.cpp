#include <ConnectionManager.h>
#include <assert.h>
#include <QDebug>
#include <QtNetwork/QHostInfo>
#include <QtNetwork/QNetworkInterface>

namespace AirBoardApp
{
    ConnectionManager::ConnectionManager(QObject* parent):
    QObject(parent),
    _udpSkt(new QUdpSocket()),
    _broadcastTimer(new QTimer()),
    _unprocessedMessageData("")
    {}

    ConnectionManager::~ConnectionManager(){}

    void
    ConnectionManager::initialize()
    {
        // Set a name for the socket obj
        _udpSkt->setObjectName("_udp_socket_ab");

        bool connected = connect(_broadcastTimer,
                                 SIGNAL(timeout()),
                                 this,
                                 SLOT(_broadcast()));

        assert(connected);

        connected = connect(_broadcastTimer,
                                 SIGNAL(timeout()),
                                 this,
                                 SLOT(_updateDeviceListModel()));

        assert(connected);

        connected = connect(_udpSkt,
                            SIGNAL(readyRead()),
                            this,
                            SLOT(_handleBroadcastResponse()),
                            Qt::DirectConnection);
        assert(connected);

        connected = _udpSkt->bind(BROADCASTING_PORT,
                                             QUdpSocket::ShareAddress | QUdpSocket::ReuseAddressHint);
        assert(connected);

        _broadcastTimer->setInterval(1000);
        //_broadcastTimer->start();
    }

    void
    ConnectionManager::_updateDeviceListModel(){
        _clearDiscoveredDevicesList();
        QMap<QString, AirBoardDevice* >::const_iterator it;
        for(it = _discoveredDevices.begin(); it != _discoveredDevices.end(); ++it)
        {
            AirBoardDevice* device = it.value();

            // Checking whether device is online
            QDateTime currTime = QDateTime::currentDateTime();
            bool isOnline = true;
            if(device->lastSeen.addSecs(DEVICE_STATUS_EXPIRY_TIME) < currTime)
            {
                isOnline = false;
            }
            _addNewDiscoveredDeviceIntoModel(device->name, device->ipAddr, device->version, device->lastSeen.toString(), isOnline);
        }
    }

    void
    ConnectionManager::broadcastMessage(const QString& msg)
    {
        QByteArray datagram = msg.toStdString().c_str();
        _udpSkt->writeDatagram(datagram.data(), datagram.size(),
                                            QHostAddress::Broadcast, BROADCASTING_PORT);
    }

    void
    ConnectionManager::subscribeRemoteWBoard(QObject* qobject)
    {
        if(qobject) {
            _subscriberList.insert(qobject);
        }
    }

    void
    ConnectionManager::_broadcast()
    {
        QString req = AIRBOARD_BROADCAST_MSG;

        req += MSG_DLMTR;
        req += QHostInfo::localHostName();
        req += MSG_DLMTR;
        req += AIRBOARD_VERSION;
        req += MSG_DLMTR;
        QByteArray datagram = req.toStdString().c_str();

        _udpSkt->writeDatagram(datagram.data(), datagram.size(),
                                            QHostAddress::Broadcast, BROADCASTING_PORT);
    }

    void
    ConnectionManager::_handleBroadcastResponse()
    {
        while (_udpSkt->hasPendingDatagrams())
        {
            QByteArray datagram;
            datagram.resize(_udpSkt->pendingDatagramSize());
            QHostAddress senderIP;
            quint16 senderPort;
            _udpSkt->readDatagram(datagram.data(), datagram.size(), &senderIP, &senderPort);

            QString incomingMessage = QString(datagram);

            _unprocessedMessageData += incomingMessage;

            while(_unprocessedMessageData.contains(MSG_DLMTR))
            {
                QString readableSection = _unprocessedMessageData.section(MSG_DLMTR, 0, 0);
                // Do what we like to do with the message
                bool validMessage = _processBroadcastResponse(readableSection, senderIP.toString());
                if(!validMessage)
                {
                    qCritical() << "Invalid message string received :" << incomingMessage << endl;
                    assert(false);
                }
                _unprocessedMessageData = _unprocessedMessageData.section(MSG_DLMTR, 1);
            }
        }
    }

    bool
    ConnectionManager::_processBroadcastResponse(const QString& data, const QString& ipAddr)
    {
        // qDebug() << "Received: " << data << " from IP:" << ipAddr;

        if(QNetworkInterface::allAddresses().indexOf(QHostAddress(ipAddr)) != -1 ||
           ipAddr.contains("127.0.0"))
        {
            // We pretty much dont want this address :)
            return true;
        }

        QString msgHead = data.section(MSG_DATA_DLMTR, 0, 0);
        if(msgHead=="B")
        {
            // TODO: Identify the WBoard and send data to subscribers here

            if(!_subscriberList.isEmpty())
            {
                QObject* subscriber;
                foreach (subscriber, _subscriberList)
                    subscriber->setProperty("msgStr", QVariant(data));
            }
        }

        // Handle draw messages here

        return true;

        // TODO:
        // enumerate Whiteboard broadcast list
        QString deviceName, version;
        bool validMsg = _parseBroadcastMsg(data, deviceName, version);

        if(validMsg)
        {
            if(_discoveredDevices.contains(ipAddr))
            {
                AirBoardDevice* device = _discoveredDevices.value(ipAddr);
                if(device)
                {
                    QDateTime currTime = QDateTime::currentDateTime();
                    device->name = deviceName;
                    device->version = version;
                    device->lastSeen = currTime;
                }
            }
            else
            {
                QDateTime currTime = QDateTime::currentDateTime();
                _addNewDevice(ipAddr, deviceName, version, currTime);
            }
        }

    }

    const QString
    ConnectionManager::_createBroadcastMsg()
    {
        QString req = AIRBOARD_BROADCAST_MSG;
        req += MSG_DLMTR;
        req += QHostInfo::localHostName();
        req += MSG_DLMTR;
        req += AIRBOARD_VERSION;
        req += MSG_DLMTR;
        return req;
    }

    bool
    ConnectionManager::_parseBroadcastMsg(const QString& infoStr, QString& name, QString& version)
    {
        QStringList dataItems = infoStr.split("#", QString::SkipEmptyParts);

        // We expect atleast 3 values
        // And cross check the msg head
        if(dataItems.size() >= 3 &&
           (dataItems[0].compare(QString(AIRBOARD_BROADCAST_MSG)) == 0))

        {
            name = dataItems[1];
            version = dataItems[2];

            // confirm that we received valid data
            return true;
        }
        else
        {
            // data received is invalid
            return false;
        }
    }

    void
    ConnectionManager::_addNewDevice(const QString& ipAddr, const QString& name, const QString& version, QDateTime createdTime)
    {
        AirBoardDevice* newDevice = new AirBoardDevice();
        newDevice->name = name;
        newDevice->ipAddr = ipAddr;
        newDevice->version = version;
        newDevice->lastSeen = createdTime;
        _discoveredDevices[ipAddr] = newDevice;
    }

    void
    ConnectionManager::_clearDiscoveredDevicesList()
    {
        /*
        if (_connectionMgrItem)
        {
            QVariant returnedValue;
            QMetaObject::invokeMethod(_connectionMgrItem, "clearDeviceList",
                                      Q_RETURN_ARG(QVariant, returnedValue));
        }
        */
    }

    void
    ConnectionManager::_addNewDiscoveredDeviceIntoModel(const QString& deviceName, const QString& ipAddr,  const QString& version,  const QString& lastSeenTime, bool isOnline)
    {
        QString suffix = "";
        if(QNetworkInterface::allAddresses().indexOf(QHostAddress(ipAddr)) != -1)
        {
            suffix = QString(" (Myself)");
        }

        /*
        if (_connectionMgrItem)
        {
            QVariant returnedValue;
            QVariant deviceNameData = QVariant(deviceName + suffix);
            QVariant versionData = QVariant(version);
            QVariant ipAddrData = QVariant(ipAddr);
            QVariant lastSeenTimeData = QVariant(lastSeenTime);
            QVariant isOnlineData = QVariant(isOnline ? 1: 0);
            QMetaObject::invokeMethod(_connectionMgrItem, "addDeviceIntoTheList",
                                      Q_RETURN_ARG(QVariant, returnedValue),
                                      Q_ARG(QVariant, deviceNameData),
                                      Q_ARG(QVariant, ipAddrData),
                                      Q_ARG(QVariant, versionData),
                                      Q_ARG(QVariant, lastSeenTimeData),
                                      Q_ARG(QVariant, isOnlineData));
        }
        */
    }
}

