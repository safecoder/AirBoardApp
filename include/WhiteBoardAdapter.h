/*********************************************
*****                              ***********
***     Author: Safeer Chonengal    **********
***     Application: AirBoard    **********
***     Date: 2016_04_11            **********
*****                           **************
***********************************************/
#ifndef _AIRBOARD_WHITEWHITEBOARDADAPTER
#define _AIRBOARD_WHITEWHITEBOARDADAPTER

#include <IWhiteBoardAdapter.h>
#include <QObject>
#include <QQmlApplicationEngine>
#include <QString>

namespace AirBoardApp
{
    /// \class  WhiteBoardAdapter
    /// \brief  Implements WhiteBoardAdapter
    class WhiteBoardAdapter:    public QObject,
                                public IWhiteBoardAdapter
    {
    Q_OBJECT
    public:
        WhiteBoardAdapter(QObject* parent=NULL);
        ~WhiteBoardAdapter();

        /// \brief  Initialize function
        void initialize(QQmlApplicationEngine* engine, QObject* parentObj);

        /// \brief  Set UUID string
        void setUuid(const QString& id);

        /// \brief  Get UUID string
        const QString getUuid() const;

        /// \brief  Set name
        void setName(const QString& name);

        /// \brief  Get name
        const QString getName() const;

        /// \brief  Set connection Manager
        void setConnectionManager(ConnectionManager* connectionManager);

    private slots:

        /// \brief  Handler for the message from drawing
        void _handleMessage(QString msgString);

        /// \brief  Handles broadcast flag changes from Gui
        void _handleBroadcastFlagChanged(bool flag);

        /// \brief  Handles Subscription flag changes from Gui
        void _handleSubscriptionFlagChanged(bool flag);

    private:

        /// \brief  Connect all Qt signals associated with Gui object
        void _connectSignals();

        /// \brief  Qml Application Engine
        QQmlApplicationEngine* _engine;

        /// \brief  Temporary Uuid of this instance for later identification
        QString _uuid;

        /// \brief  Name assigned to this AirBoard
        QString _name;

        /// \brief  Gui component of WhiteBoard
        QObject* _whiteBoardGui;

        /// \brief  Connection Manager for network communication
        ConnectionManager* _connectionManager;
    };
}

#endif //_AIRBOARD_WHITEWHITEBOARDADAPTER
