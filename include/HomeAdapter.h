/*********************************************
*****                              ***********
***     Author: Safeer Chonengal    **********
***     Application: AirBoard    **********
***     Date: 2016_04_11            **********
*****                           **************
***********************************************/
#ifndef _AIRBOARD_HOMEADAPTER
#define _AIRBOARD_HOMEADAPTER

#include <IHomeAdapter.h>
#include <ConnectionManager.h>
#include <QSharedPointer>
#include <QObject>
#include <QMap>

namespace AirBoardApp
{
    class IWhiteBoardAdapter;

    /// \class  HomeAdapter
    /// \brief  Implements HomeAdapter
    class HomeAdapter:    public QObject,
                          public IHomeAdapter
    {
    Q_OBJECT
    public:
        HomeAdapter(QObject* parent=NULL);
        ~HomeAdapter();

        /// \brief  Initialize function
        void initialize(QQmlApplicationEngine* engine);

        /// \brief  Set GUI object
        void setGUIObject(QObject* qObj);

        /// \brief  Get GUI object
        QObject* getGUIObject() const;

        /// \brief  Set UUID string
        void setUuid(const QString& id);

        /// \brief  Get UUID string
        const QString getUuid() const;

        /// \brief  Launch the Window matching Uuid
        ///         If not specified launch home
        void launchWindow(const QString& uuid = "");

    private slots:
        /// \brief  Slot: Create new AirBoard
        void _handleCreateAirBoardRequest(const QString& inputJson);

    private:

        /// \brief  Gui Object associated with
        QObject* _homeQmlComponent;

        /// \brief  Qml Application Engine
        QQmlApplicationEngine* _engine;

        /// \brief  List of WhiteBoard Adapters
        QMap< QString, QSharedPointer<IWhiteBoardAdapter> > _whiteBoardList;

        /// \brief  Temporary Uuid of this instance for later identification
        QString _uuid;

        /// \brief  Connection Mananger
        QSharedPointer<ConnectionManager> _connectionManager;
    };
}

#endif //_AIRBOARD_HOMEADAPTER
