#ifndef _CONNECTION_MANAGER_AIRBOARD_
#define _CONNECTION_MANAGER_AIRBOARD_

#include <QObject>
#include <QtNetwork/QTcpSocket>
#include <QtNetwork/QTcpServer>
#include <QtNetwork/QUdpSocket>
#include <QTimer>
#include <QDateTime>
#include <QSet>

#define DEVICE_STATUS_EXPIRY_TIME 3 // In sec
#define BROADCASTING_PORT 5981

#define AIRBOARD_VERSION "1.0"
#define AIRBOARD_BROADCAST_MSG "HELLO_WBOARD_ONLINE"
#define MSG_DLMTR "#"
#define MSG_DATA_DLMTR ":"

namespace AirBoardApp
{
    // Airboard Device we discover
    struct AirBoardDevice
    {
        QString name;
        QString ipAddr;
        QString version;
        QDateTime lastSeen;
    };

    class ConnectionManager: public QObject
    {
    Q_OBJECT
    public:

        ConnectionManager(QObject* parent=0);
        ~ConnectionManager();

        /// \brief  Initialize method
        void initialize();

        /// \brief  Broadcast a message string over UDP
        void broadcastMessage(const QString& msg);

        /// \brief  Subscribe to remote WBoard
        void subscribeRemoteWBoard(QObject* qobject);

    private slots:

        /// \brief  broad cast predefined signals
        void _broadcast();

        /// \brief  update the list of the discovered devices model
        void _updateDeviceListModel();

        /// \brief  handles broad cast response
        void _handleBroadcastResponse();

    private:

        /// \brief  Clears the list of the discovered devices
        void _clearDiscoveredDevicesList();

        /// \brief  Add new device into the ListModel of the QML Item
        void _addNewDiscoveredDeviceIntoModel(const QString& deviceName, const QString& version,  const QString& ipAddr,  const QString& lastSeenTime, bool isOnline);

        /// \brief  Precess the string response received from broadcast socket
        bool _processBroadcastResponse(const QString& data, const QString& ipAddr);

        /// \brief  serialize the AirBoard Info for broadcasting
        const QString _createBroadcastMsg();

        /// \brief  deserialize the AirBoard Info
        bool _parseBroadcastMsg(const QString& infoStr, QString& name, QString& version);

        /// \brief  adds new AirBoard device into the list
        void _addNewDevice(const QString& ipAddr, const QString& name, const QString& version, QDateTime createdTime);

        // UDP Socket
        QUdpSocket* _udpSkt;

        /// \brief  Unprocessed string data
        QString _unprocessedMessageData;

        // broad cast timer
        QTimer* _broadcastTimer;

        // List of discovered AirBoards
        QMap<QString, AirBoardDevice* > _discoveredDevices;

        /// \brief  List of Subscribers qml object
        QSet<QObject*> _subscriberList;
    };

}
#endif // _CONNECTION_MANAGER_AIRBOARD_
