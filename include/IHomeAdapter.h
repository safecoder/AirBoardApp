/*********************************************
*****                              ***********
***     Author: Safeer Chonengal    **********
***     Application: AirBoard    **********
***     Date: 2016_04_11            **********
*****                           **************
***********************************************/
#ifndef I_AIRBOARD_HOMEADAPTER
#define I_AIRBOARD_HOMEADAPTER

#include <QObject>

class QQmlApplicationEngine;

namespace AirBoardApp
{
    /// \class  IHomeAdapter
    /// \brief  Interface for HomeAdapter
    class IHomeAdapter
    {
    public:
        virtual ~IHomeAdapter(){}

        /// \brief  Initialize function
        virtual void initialize(QQmlApplicationEngine* engine) = 0;

        /// \brief  Set GUI object
        virtual void setGUIObject(QObject* qObj) = 0;

        /// \brief  Get GUI object
        virtual QObject* getGUIObject() const = 0;

        /// \brief  Set UUID string
        virtual void setUuid(const QString& id) = 0;

        /// \brief  Get UUID string
        virtual const QString getUuid() const = 0;

        /// \brief  Launch the Window matching Uuid
        ///         If not specified launch home
        virtual void launchWindow(const QString& uuid = "") = 0;
    };
}

#endif // I_AIRBOARD_HOMEADAPTER
