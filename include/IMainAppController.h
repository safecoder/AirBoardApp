/*********************************************
*****                              ***********
***     Author: Safeer Chonengal    **********
***     Application: AirBoardApp    **********
***     Date: 03/04/2016            **********
*****                           **************
***********************************************/
#ifndef _IMAINAPPCONTROLLER_AIRBOARD_
#define _IMAINAPPCONTROLLER_AIRBOARD_

#include <QQmlApplicationEngine>

namespace AirBoardApp
{
    /// \class  IMainAppController
    /// \brief  Interface for MainAppController
    class IMainAppController
    {
    public:

        /// \enum   AppState
        /// \brief  MainAppController work as a State Machine
        enum AppState
        {
            UNDEFINED,
            APP_LOADING,
            LAUNCH_HOME,
            APP_LOADING_COMPLETE,
            AIRBOARD_HOME,
            AIRBOARD_DRAWING,
            SHARING_OPTIONS_PAGE
        };

        virtual ~IMainAppController(){}

        /// \brief  Initialize function
        virtual void initialize(QQmlApplicationEngine* engine) = 0;

    };
}

#endif //_IMAINAPPCONTROLLER_AIRBOARD_
