/*********************************************
*****                              ***********
***     Author: Safeer Chonengal    **********
***     Application: AirBoardApp    **********
***     Date: 03/04/2016            **********
*****                           **************
***********************************************/
#ifndef _MAINAPPCONTROLLER_AIRBOARD_
#define _MAINAPPCONTROLLER_AIRBOARD_

#include <IMainAppController.h>
#include <QObject>
#include <QTimer>
#include <QList>

namespace AirBoardApp
{
    class IHomeAdapter;

    /// \class  MainAppController
    /// \brief  Implements MainAppController
    class MainAppController:    public QObject,
                                public IMainAppController
    {
    Q_OBJECT
    public:

        MainAppController(QObject* parent=NULL);
        ~MainAppController();

        /// \brief  Initialize function
        void initialize(QQmlApplicationEngine* engine);

    private slots:
        /// \brief  Update the App states
        void _update();

    private:
        /// \brief  Loads home Adapter and Gui component
        void _loadHomeComponent();

        /// \brief  Load components one by one
        ///         One call of this method will load one component and increment the _loaderList
        ///         Returns boolean indicating whether loading is complete
        bool _loadComponent();

        /// \brief  Apply a state to the App
        AppState _applyState(const AppState& state);

        /// \brief  Qml Application Engine
        QQmlApplicationEngine* _engine;

        /// \brief  State that now App should go into
        AppState _currentState;

        /// \brief  Main Timer for the App
        QTimer _mainTimer;

        /// \brief  loader list of components
        ///         Value: name of the component
        QList<QString> _loaderList;

        /// \brief Iterator for tracking the currently loading component
        QList<QString>::const_iterator _loaderItr;

        /// \brief  Home Adapter
        QSharedPointer<IHomeAdapter> _homeAdapter;
    };
}

#endif //_MAINAPPCONTROLLER_AIRBOARD_
