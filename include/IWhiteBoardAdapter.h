/*********************************************
*****                              ***********
***     Author: Safeer Chonengal    **********
***     Application: AirBoard    **********
***     Date: 2016_04_11            **********
*****                           **************
***********************************************/
#ifndef I_AIRBOARD_WHITEWHITEBOARDADAPTER
#define I_AIRBOARD_WHITEWHITEBOARDADAPTER

#include <QString>

class QQmlApplicationEngine;
class QObject;

namespace AirBoardApp
{
    class ConnectionManager;

    /// \class  IWhiteBoardAdapter
    /// \brief  Interface for WhiteBoardAdapter
    class IWhiteBoardAdapter
    {
    public:
        virtual ~IWhiteBoardAdapter(){}

        /// \brief  Initialize function
        virtual void initialize(QQmlApplicationEngine* engine, QObject* parentObj) = 0;

        /// \brief  Set UUID string
        virtual void setUuid(const QString& id) = 0;

        /// \brief  Get UUID string
        virtual const QString getUuid() const = 0;

        /// \brief  Set name
        virtual void setName(const QString& name) = 0;

        /// \brief  Get name
        virtual const QString getName() const = 0;

        /// \brief  Set connection Manager
        virtual void setConnectionManager(ConnectionManager* connectionManager) = 0;

    };
}

#endif // I_AIRBOARD_WHITEWHITEBOARDADAPTER
