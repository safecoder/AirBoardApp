/*********************************************
*****                              ***********
***     Author: Safeer Chonengal    **********
***     Application: AirBoardApp    **********
***     Date: 03/04/2016            **********
*****                           **************
***********************************************/
#ifndef _APPCONSTANTS_AIRBOARD_
#define _APPCONSTANTS_AIRBOARD_


    #define APP_NAME "AirBoard"
    #define APP_VERSION "1.0"
    #define AIRBOARD_THEME_COLOR "#505856"
    #define AB_LOG_OUTPUT_FILE_NAME_PREFIX "AirBoardLog"


#endif //_APPCONSTANTS_AIRBOARD_
