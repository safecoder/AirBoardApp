import QtQuick 2.4
import QtQuick.Window 2.2

Item {
    id: deviceProperyValue

    property bool initialized: false
    property var propertyValue;
    property int phoneIndex: 0;
    property int desktopIndex: 1;

    // Get GUI layout params
    // based on device name
    function val(token)
    {
        if(!initialized)
        {
            propertyValue = {};

            // Dimensional params in mm
            // Array values are values corresp. to a device
            // As per the indices we define
            propertyValue["button-text-size"] = [4, 5];
            propertyValue["text-input-height"] = [5, 8];
            propertyValue["text-input-width"] = [40, 60];
            propertyValue["small-text-size"] = [2, 4];
            propertyValue["medium-text-size"] = [3, 8];
            propertyValue["big-text-size"] = [6, 15];
            propertyValue["window-icon-size"] = [5, 10];
            propertyValue["settings-icon-size"] = [10, 15];
            propertyValue["open-windows-panel-height"] = [10, 15];
            propertyValue["color-box-size"] = [10, 15];
            propertyValue["stroke-size-option-1"] = [0.2, 0.4];
            propertyValue["stroke-size-option-2"] = [0.5, 1];
            propertyValue["stroke-size-option-3"] = [1, 2];
            propertyValue["stroke-size-option-4"] = [2, 4];
            propertyValue["stroke-size-option-5"] = [4, 6];
            propertyValue["stroke-size-option-6"] = [6, 8];

            initialized=true;
        }

        if(typeof device === "undefined" ||
          !(token in propertyValue))
        {
            console.log("Device type or Token not found :" + token);
            return -1;
        }

        // layout index to be decided
        var index;

        switch(device)
        {
        case "android-phone":
        case "windows-phone":
            index = phoneIndex;
            break;
        case "windows-desktop":
        case "android-tablet":
            index = desktopIndex;
            break;
        default:
            index = phoneIndex;
            break;
        }

        var returnVal = propertyValue[token][index] * Screen.pixelDensity;
        // console.log("t: " + token + " i: " + index + " r: " + returnVal)
        return returnVal;
    }
}
