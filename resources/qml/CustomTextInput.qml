import QtQuick 2.4
import QtQuick.Window 2.2

// Abstraction of Text input box

Rectangle {
    id: textInputWrapper

    // use width outside to customize
    width: dpv.val("text-input-width");
    height: dpv.val("text-input-height");

    // Alias to input string
    property alias inputText: inputBox.text

    // label
    property string label: "Label"

    // Default color for the button
    color: Qt.lighter(airboardColor, 3)

    radius: 5
    border.width: 1
    border.color: Qt.darker(airboardColor, 1.5)

    // Submit signal
    signal submitValue(string val);

    // TextInput
    TextInput
    {
        id: inputBox
        property int margins: 3
        anchors.fill:parent
        anchors.leftMargin: margins
        anchors.rightMargin: margins
        font.pixelSize: textInputWrapper.height*0.8
        color: "#555";
        mouseSelectionMode: TextInput.SelectCharacters
        selectByMouse: true
        activeFocusOnPress: true;
        onAccepted:
        {
            submitValue(text);
        }

        Text{
            id: labelText
            text: label
            visible: {
                if(focus || activeFocus)
                {
                    return false;
                }

                return inputBox.text === "";
            }
            anchors.fill: parent
            font.pixelSize: inputBox.font.pixelSize
            color: inputBox.color
            font.italic: true
            anchors.leftMargin: inputBox.margins
            opacity: 0.3
        }
    }
}
