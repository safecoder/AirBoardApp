import QtQuick 2.4
import QtQuick.Window 2.2

// Abstraction of button

Rectangle {
    id: buttonWrapper

    // use width outside only when isFixedWidth is set to true
    width: {
        return isFixedWidth ? 100 : labelTxt.width + 20;
    }
    height: labelTxt.height + 10;

    property int textSize: dpv.val("button-text-size");

    // whether the button should take a fixed width
    // or width is acc. to the label width
    property bool isFixedWidth: false

    // button label
    property string label: ""

    // Default color for the button
    color: highlight ? highlightColor : Qt.darker(airboardColor, 1.5)

    property color highlightColor: "#668"

    property bool highlight: false

    // Opacity value
    opacity: 0.8

    border.width: 1
    border.color: Qt.lighter(airboardColor, 2)

    // Click signal
    signal buttonClicked();

    // Text label
    Text{
        id: labelTxt
        text: label
        width: isFixedWidth ? parent.width*0.8 : contentWidth;
        anchors.centerIn: parent
        font.pixelSize: textSize
        font.weight: Font.Bold
        color: Qt.lighter(airboardColor, 2);
    }

    // Button mouse area
    MouseArea{
        cursorShape: Qt.PointingHandCursor
        anchors.fill: parent
        onClicked: {
            // console.log(buttonWrapper.width + ", " + labelTxt.width)
            buttonClicked();
        }
    }
}
