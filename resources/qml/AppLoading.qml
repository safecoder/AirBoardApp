import QtQuick 2.4
import QtQuick.Window 2.2

// Main Window of the Application
// It has a child Rect "MainContainer" that holds
// currently displayed Gui item
Window {
    visible: true
    width: Screen.desktopAvailableWidth;
    height: Screen.desktopAvailableHeight;

    Component.onCompleted:
    {
        // Once we are ready we show App in fullscreen
        // showFullScreen();
    }

    Rectangle{
        objectName: "MainContainer"
        anchors.fill: parent;
        property bool loadingDone: false;
        color: typeof airboardColor !== "undefined" ?
                   airboardColor : "#555";

        // Button to show loading
        CustomButton{
            id: loading;
            visible: !parent.loadingDone
            label: "Loading...";
            border.width: 0;
            textSize: 50
            anchors.centerIn: parent;
            isFixedWidth: false;
            onButtonClicked: label = "Wait...";
        }
    }
}
