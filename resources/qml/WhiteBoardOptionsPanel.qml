import QtQuick 2.4

Rectangle{
    id: optionsPanelWrapper
    width: parent===null ? 200 : parent.width;
    height: parent===null ? 200 : parent.height;
    color: "transparent"
    objectName: "_whiteboardoptions_"
    visible: false;

    onVisibleChanged: {
        if(visible)
        {
            visibleItemName=mainOptionList.objectName;
        }
    }

    //  This is the object name of the item that is
    //  Made visible currently
    property string visibleItemName: "undefined"

    // Currently selected color for drawing
    // Default is kept white
    property string selectedColor: "white"

    // Currently selected stroke size for drawing
    property int selectedStrokeSize: dpv.val("stroke-size-option-3");

    //Overlay
    Rectangle{
        anchors.fill: parent
        color: "black"
        opacity: 0.5
    }

    MouseArea{
        anchors.fill: parent
        onClicked: exitSettingsWindow();
    }

    CustomButton{
        label: "White Board Settings";
        visible: mainOptionList.visible
        anchors.bottom: mainOptionList.top
        anchors.horizontalCenter: mainOptionList.horizontalCenter
        anchors.bottomMargin: 25
        border.width: 0
    }

    CustomButton{
        visible: visibleItemName !== mainOptionList.objectName;
        label: "Back"
        radius: 10;
        anchors.left: optionsPanelWrapper.left
        anchors.top: optionsPanelWrapper.top
        anchors.margins: 50
        onButtonClicked: {
            visibleItemName=mainOptionList.objectName;
        }
    }

    Text{
        color: "#EEE"
        text: "Tap anywhere<br> to hide options"
        anchors.left: mainOptionList.right
        anchors.verticalCenter: mainOptionList.verticalCenter
    }

    // Main option list
    GridView{
        id: mainOptionList
        objectName: "_MainOptionsList_"
        visible: visibleItemName===objectName
        width: optionsPanelWrapper.width*0.6
        height: optionsPanelWrapper.height*0.5
        anchors.centerIn: parent;

        flickableDirection: GridView.HorizontalFlick
        boundsBehavior: GridView.StopAtBounds
        clip: true

        cellWidth: width/3
        cellHeight: height/4

        model: ListModel{
            id: optionsModel
            ListElement{ name: "Color"}
            ListElement{ name: "Stroke"}
            ListElement{ name: "Broadcast"}
            ListElement{ name: "Subscribe"}
            ListElement{ name: "Minimize"}
            ListElement{ name: "Exit W-Brd"}
        }

        delegate: Component{
            Item{
                width: mainOptionList.cellWidth
                height: mainOptionList.cellHeight

                CustomButton{
                    width: parent.width-40
                    label: name

                    highlight: {
                        switch(name){
                        case "Broadcast":
                            return whiteboardWrapper.broadcastEnabled;
                        case "Subscribe":
                            return whiteboardWrapper.subscriptionEnabled;
                        }

                        return false;
                    }

                    onButtonClicked: {
                        if(name === "Color")
                        {
                            visibleItemName=colorsPanel.objectName;
                        }
                        else if(name === "Stroke")
                        {
                            visibleItemName=strokeSelectionPanel.objectName;
                        }
                        else if(name === "Minimize")
                        {
                            whiteboardWrapper.parent.currentWindowUuid = whiteboardWrapper.parent.uuid
                            exitSettingsWindow();
                        }
                        else if(name === "Broadcast")
                        {
                            whiteboardWrapper.broadcastEnabled = !whiteboardWrapper.broadcastEnabled;
                        }
                        else if(name === "Subscribe")
                        {
                            whiteboardWrapper.subscriptionEnabled = !whiteboardWrapper.subscriptionEnabled;
                        }
                    }
                }
            }
        }
    }

    // Colors panel
    ColorPicker{
        id: colorsPanel
    }

    StrokeSizePanel{
        id: strokeSelectionPanel
    }
}
