import QtQuick 2.4
import QtQuick.Window 2.2

// Home GUI
// Lists all AirBoards previously created
// Create new AirBoard by clicking "+" Button
Rectangle{
    id: homeWindowWrapper
    objectName: "_home_window_"
    anchors.fill: parent
    color: "transparent"

    //  uuid
    property string uuid: "_undefined"

    visible: uuid === currentWindowUuid;

    // function to launch Create-AirBoard-Inputs-panel
    function launchCreateNewAirBoardPanel()
    {
        createAirBoardInputPanel.visible=true;
        abListGrid.visible=false;
    }

    // function to hide Create-AirBoard-Inputs-panel
    function hideCreateNewAirBoardPanel()
    {
        createAirBoardInputPanel.visible=false;
        abListGrid.visible=true;
    }

    // Exit the settings window
    function exitSettingsWindow()
    {
        settingsMode = false;
    }

    // Settings panel
    HomeSettingsPanel
    {
        id: optionsOverlay
        visible: {
            // Visible when Currently Visible window is this
            // And settings mode is ON
            return homeWindowWrapper.visible &&
                   settingsMode;
        }
        z: 3
        anchors.fill: parent
    }

    // Title for AirBoard List
    Text {
        id: titleText
        text: createAirBoardInputPanel.visible?
                  "Create W-Board" : "List of W-Boards";
        color: Qt.lighter(airboardColor, 3);
        font.pixelSize: dpv.val("medium-text-size");
        anchors.bottom: abListPanel.top
        anchors.horizontalCenter: abListPanel.horizontalCenter
    }

    Rectangle {
        id: abListPanel
        width: parent.width*0.7;
        height: parent.height*0.7;
        anchors.centerIn: parent;
        color: "transparent"
        border.width: createAirBoardInputPanel.visible ? 1 : 0;
        border.color: Qt.lighter(airboardColor, 1.5)
        radius: 5

        Rectangle
        {
            id: createAirBoardInputPanel
            visible: false
            anchors.centerIn: parent
            width: parent.width*0.8
            height: parent.height
            color: "transparent"

            Column{
                anchors.centerIn: parent
                spacing: 25

                Text {
                    id: panelTitle
                    text: "NEW W-BOARD"
                    color: Qt.lighter(airboardColor, 3);
                    font.pixelSize: dpv.val("medium-text-size")
                }

                Rectangle{
                    color: "transparent"
                    width: createAirBoardInputPanel.width
                    height: airboardName.height+30

                    Text {
                        anchors.left: parent.left
                        anchors.verticalCenter: airboardName.verticalCenter
                        text: "Board Name: "
                        color: Qt.lighter(airboardColor, 3);
                        font.pixelSize: dpv.val("medium-text-size")
                    }

                    CustomTextInput{
                        id: airboardName
                        anchors.right: parent.right
                        label: ""
                        inputText: "name"
                    }
                }

                Rectangle{
                    id: emailField
                    color: "transparent"
                    width: createAirBoardInputPanel.width
                    height: emailAddress.height+30

                    Text {
                        anchors.left: parent.left
                        anchors.verticalCenter: emailAddress.verticalCenter
                        text: "Email Address: "
                        color: Qt.lighter(airboardColor, 3);
                        font.pixelSize: dpv.val("medium-text-size")
                    }

                    CustomTextInput{
                        id: emailAddress
                        anchors.right: parent.right
                        label: ""
                        inputText: "name@abc.com"
                    }
                }

                CustomButton{
                    label: "CREATE W-BOARD"
                    anchors.right: emailField.right
                    onButtonClicked: {

                        var inputObj = {
                            "name": airboardName.inputText,
                            "email": emailAddress.inputText
                        };
                        // Send signal to create new AirBoard
                        createAirBoard(JSON.stringify(inputObj));
                        homeWindowWrapper.hideCreateNewAirBoardPanel();
                    }
                }

                CustomButton{
                    anchors.right: emailField.right
                    label: "CANCEL"
                    onButtonClicked: {
                        homeWindowWrapper.hideCreateNewAirBoardPanel();
                    }
                }
            }
        }

        // List of All AirBoard Items previously used
        GridView{
            id: abListGrid
            visible: true
            width: parent.width-5;
            height: parent.height-5;
            anchors.centerIn: parent

            flickableDirection: GridView.HorizontalFlick
            boundsBehavior: GridView.StopAtBounds
            clip: true

            cellWidth: width/3.05;
            cellHeight: height/3.05;

            model: ListModel{
                ListElement{ type:"ab"; name:"myWriting-1"}
                ListElement{ type:"ab"; name:"myWriting-2-asncddjksdlskd"}
                ListElement{ type:"ab"; name:"myWriting-XX"}
                ListElement{ type:"ab"; name:"teaching-1"}
                ListElement{ type:"create"; name: "create"}
            }

            delegate:
                Component{
                    Rectangle{
                        id: tile
                        width: abListGrid.cellWidth;
                        height: abListGrid.cellHeight;
                        color: "transparent"
                        property bool isCreateTile: type === "create"
                        Rectangle{
                            width: parent.width - 5;
                            height: parent.height - 5;
                            anchors.centerIn: parent;
                            color: Qt.lighter(airboardColor, 1.5);
                            border.color: "white"
                            border.width: tile.isCreateTile ? 1 : 0

                            Text {
                                id: abName
                                visible: !tile.isCreateTile
                                text: {
                                    if(name.length < 10)
                                    {
                                        return name;
                                    }
                                    else
                                    {
                                        return name.substring(0, 10)+"..";
                                    }
                                }
                                color: Qt.darker(airboardColor, 2);
                                font.pixelSize: dpv.val("small-text-size");
                                anchors.centerIn: parent
                            }

                            Text {
                                id: createNew
                                visible: tile.isCreateTile
                                text: "+"
                                color: Qt.darker(airboardColor, 2);
                                font.pixelSize: parent.height*0.4
                                font.bold: true
                                anchors.centerIn: parent
                            }

                            MouseArea {
                                id: tileMouseArea
                                anchors.fill: parent
                                cursorShape: Qt.PointingHandCursor
                                onClicked:
                                {
                                    if(tile.isCreateTile)
                                    {
                                        console.log("New AirBoard Registration");
                                        homeWindowWrapper.launchCreateNewAirBoardPanel();
                                    }
                                }
                            }
                        }
                    }
                }
        }
    }

    // Button to quit the App
    CustomButton{
        id: quitBtn
        label: "QUIT"
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.margins: 20
        radius: 10
        border.width: 0
        isFixedWidth: false
        onButtonClicked: Qt.quit();
    }
}

