import QtQuick 2.4
import QtQuick.Window 2.2

// Main Container GUI
// All windows(Home and WhiteBoards) are parented by this
Rectangle{
    id: mainWrapper
    width: parent===null ? 200: parent.width;
    height: parent===null ? 400: parent.height;
    color: "transparent"

    DevicePropertyValue { id: dpv; }
    property alias dpv: dpv

    // uuid of the home
    property alias uuid: homeWindowWrapper.uuid;

    // Currently open window Uuid
    property string currentWindowUuid: "undefined"

    // Signal for creating new AirBoard
    signal createAirBoard(string inputJson);

    // Flag that indicates the settings window is currently shown
    // Settings window might belong to Home window or any WhiteBoards
    property bool settingsMode: false

    // We keep list of Whiteboards that are open
    // Also we create quick access from one another
    onCurrentWindowUuidChanged: {
        openWindowsModel.update();
    }

    // ListModel for all Open Windows
    ListModel{
        id: openWindowsModel

        // Function to update windows ListElement
        function update()
        {
            clear();
            var childItems = mainWrapper.children;
            for(var i=0; i<childItems.length; i++)
            {
                if(childItems[i].objectName === "_whiteboard_" &&
                   childItems[i].uuid !== currentWindowUuid)
                {
                    append({
                              type: "whiteboard",
                              name: childItems[i].name,
                              windowUuid: childItems[i].uuid,
                              iconUrl: childItems[i].canvasImgSrc(),
                              bgColor: childItems[i].backgroundColor
                           });
                }
                else if(childItems[i].objectName === "_home_window_")
                {
                    append({
                              type: "homewindow",
                              name: "Home",
                              windowUuid: uuid,
                              iconUrl: "qrc:///home-icon.png",
                              bgColor: "transparent"
                           });
                }
            }
        }
    }

    // A settings icon floating everywhere.
    // As when user wants to see settings, let the click
    // happen here
    Rectangle{
        id: settingsIcon
        visible: !settingsMode
        x: 50; y: mainWrapper.height-height-100;
        // Put high z as to bring it on top always
        z: 1000

        width: dpv.val("settings-icon-size")
        height: width

        radius: width/2.0;
        color: Qt.lighter(airboardColor, 1.1)
        border.width: 1
        border.color: "#AAA"
        opacity: 0.7;

        Image{
            property int imgPadding: 20
            source: "qrc:///settings.png"
            width: settingsIcon.width-imgPadding;
            height: width
            anchors.centerIn: parent
        }

        MouseArea{
            anchors.fill: parent
            cursorShape: Qt.PointingHandCursor
            drag.target: settingsIcon;
            drag.minimumX: 0
            drag.minimumY: 0
            drag.maximumX: mainWrapper.width - settingsIcon.width
            drag.maximumY: mainWrapper.height - settingsIcon.height

            propagateComposedEvents: true;

            onClicked: {
                settingsMode=true;
                //console.log("setting icon clicked..");
            }
        }
    }

    // Minimized windows are horizontally displayed here as tabs
    // We can easily switch between open windows
    Rectangle{
        id: windowsPanel
        visible: settingsMode
        anchors.bottom: mainWrapper.bottom
        anchors.bottomMargin: 100
        // Put high z as to bring it on top always
        z: 1000
        width: mainWrapper.width
        height: dpv.val("open-windows-panel-height")
        color: Qt.lighter(airboardColor, 1.1)
        border.width: 1
        border.color: "#EEE"
        opacity: 0.6

        property int flickButtonWidth: 70

        ListView{
            id: openWindowList
            width: windowsPanel.width - 2*windowsPanel.flickButtonWidth
            height: windowsPanel.height
            anchors.centerIn: windowsPanel
            orientation: ListView.Horizontal
            spacing: 25

            clip: true
            boundsBehavior: Flickable.StopAtBounds

            model: openWindowsModel

            delegate: Component{
                Rectangle{
                    width: iconImg.width+50
                    height: openWindowList.height
                    color: "transparent"

                    Rectangle{
                        id: iconImg
                        height: parent.height-10
                        width: height*1.5
                        anchors.centerIn: parent
                        color: bgColor
                        Image{
                            anchors.fill: parent
                            source: iconUrl;
                        }
                    }

                    CustomButton{
                        id: windowLauncherBtn
                        label: {
                            if( name.length < 10)
                            {
                                return name;
                            }
                            else
                            {
                                return name.substring(0, 10)+"..";
                            }
                        }
                        border.width: 0
                        anchors.bottom: iconImg.bottom
                        anchors.horizontalCenter: iconImg.horizontalCenter
                        onButtonClicked: {
                            // Make current window as this
                            currentWindowUuid = windowUuid;

                            // Make settings window disappear
                            settingsMode=false;
                        }
                    }
                }
            }
        }
    }

    // Home GUI
    // Lists all AirBoards previously created
    // Create new AirBoard by clicking "+" Button
    Home{
        id: homeWindowWrapper
        objectName: "_home_window_"
    }
}

