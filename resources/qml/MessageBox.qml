import QtQuick 2.4

// Define messages
// Sends and receives messages
// Mediator for message transactions
QtObject{
    id: msgBox
    objectName: "MessageBoxObj"

    function drawMessageStr(pt, canvasW, canvasH, sendingMode)
    {
        //console.log("sendDrawMessage");

        var ptStr = (pt.x/canvasW).toString() + ":" + (pt.y/canvasH).toString();
        var str = "B:D:" + ptStr + "#";

        return str;
    }

    function beginDrawMessageStr(strokeColor, strokeSize, sendingMode)
    {
        //console.log("sendBeginDrawMessage");
        var str = "B:S:" + strokeColor + ":" + strokeSize + "#";

        return str;
    }
}
