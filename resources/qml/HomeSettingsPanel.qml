import QtQuick 2.4

// Settings Panel for the Home Window
Rectangle{
    id: optionsPanelWrapper
    width: parent===null ? 200 : parent.width;
    height: parent===null ? 200 : parent.height;
    color: "transparent"
    objectName: "_homeoptionspanel_"
    visible: false;

    onVisibleChanged: {
        if(visible)
        {
            visibleItemName=mainOptionList.objectName;
        }
    }

    //  This is the object name of the item that is
    //  Made visible currently
    property string visibleItemName: "undefined"

    //Overlay
    Rectangle{
        anchors.fill: parent
        color: "black"
        opacity: 0.5
    }

    MouseArea{
        anchors.fill: parent
        onClicked: exitSettingsWindow();
    }

    // Main option list
    ListView{
        id: mainOptionList
        objectName: "_MainOptionsList_"
        visible: visibleItemName===objectName
        width: optionsPanelWrapper.width*0.3
        height: optionsPanelWrapper.height*0.6
        anchors.centerIn: parent;
        spacing: 20

        CustomButton{
            label: "Home Settings";
            anchors.bottom: parent.top
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottomMargin: 5
            border.width: 0
        }

        Text{
            color: "#EEE"
            text: "( Tap anywhere to hide options )"
            anchors.top: parent.bottom
        }

        Rectangle{
            anchors.fill: parent
            color: "transparent"
            radius: 5
            border.width: 1
            border.color: Qt.lighter(airboardColor, 1.5)
        }

        model: ListModel{
            id: optionsModel
            ListElement{ name: "Exit"}
        }

        delegate: Component{
            CustomButton{
                width: mainOptionList.width
                label: name
                onButtonClicked: {
                }
            }
        }
    }
}
