import QtQuick 2.4

// Stroke size selection panel
// A grid of stroke sizes user can select from
// Selected stroke size is used for drawing
GridView{
    id: strokeSizeSelectionPanelWrapper
    objectName: "_StrokesizeList_"
    visible: visibleItemName===objectName

    width: optionsPanelWrapper.width*0.6
    height: optionsPanelWrapper.height*0.7
    anchors.centerIn: parent

    cellWidth: width/6;
    cellHeight: height/3;

    onVisibleChanged: {
        if(visible && strokeSizeModel.count===0)
        {
            loadStrokeSizeOptions();
        }
    }

    function loadStrokeSizeOptions() {
        strokeSizeModel.clear();
        for(var i=1; i<=6; i++)
        {
            var sizeVal = dpv.val("stroke-size-option-" + i.toString());
            //console.log(sizeVal);
            strokeSizeModel.append(
                        {
                            size: sizeVal
                        });
        }
    }

    footer: Text{
            color: "#EEE"
            text: "<h2>Select brush size</h2>"
        }

    model: ListModel{
        id: strokeSizeModel
    }

    delegate: Component {
        Rectangle{
            id: strokeSelectionItemWrapper
            width: strokeSizeSelectionPanelWrapper.cellWidth
            height: strokeSizeSelectionPanelWrapper.cellheight
            color: "transparent"

            Rectangle {
                id: itemWrapper
                anchors.centerIn: parent
                property bool isSelected: size===selectedStrokeSize
                width: dpv.val("color-box-size");
                height: width;
                color: "transparent";
                border.width: 1;
                border.color: "#EEE";

                Rectangle{
                    anchors.centerIn: parent
                    width: size;
                    height: parent.height
                }

                MouseArea{
                    anchors.fill: parent;
                    cursorShape: Qt.PointingHandCursor
                    onClicked: {
                        selectedStrokeSize=size;
                        exitSettingsWindow();
                    }
                }
            }
        }
    }
}
