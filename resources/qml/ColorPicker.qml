import QtQuick 2.4

// Colors panel
// A grid of colors user can select from
// Selected color is used for drawing
GridView{
    id: colorsPanelWrapper
    objectName: "_ColorsList_"
    visible: visibleItemName===objectName

    width: optionsPanelWrapper.width*0.6
    height: optionsPanelWrapper.height*0.7
    anchors.centerIn: parent

    cellWidth: width/6;
    cellHeight: height/3;

    footer: Text{
            color: "#EEE"
            text: "<h2>Select color</h2>"
        }

    model: ListModel{
        id: colorsModel
        ListElement{ col:"#AAA"}
        ListElement{ col:"#EEE"}
        ListElement{ col:"#E88"}
        ListElement{ col:"#8E8"}
        ListElement{ col:"#88E"}
        ListElement{ col:"#8EE"}
        ListElement{ col:"#EE8"}
        ListElement{ col:"#E8E"}
        ListElement{ col:"#EAB"}
        ListElement{ col:"#AAE"}
        ListElement{ col:"#BAE"}
    }

    delegate: Component {
        Rectangle{
            id: colorItemWrapper
            width: colorsPanelWrapper.cellWidth
            height: colorsPanelWrapper.cellheight
            color: "transparent"
            Rectangle {
                anchors.centerIn: parent
                property bool isSelected: col===selectedColor
                width: dpv.val("color-box-size");
                height: width;
                radius: width/2.0;
                color: col;
                border.width: 1;
                border.color: isSelected ? "#000" : "#EEE";

                MouseArea{
                    anchors.fill: parent;
                    cursorShape: Qt.PointingHandCursor
                    onClicked: {
                        selectedColor=col;
                        exitSettingsWindow();
                    }
                }
            }
        }
    }
}
