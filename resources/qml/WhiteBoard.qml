import QtQuick 2.2
import QtQuick.Window 2.2

Rectangle{
    id: whiteboardWrapper
    anchors.fill: parent
    color: "#000"
    objectName: "_whiteboard_"
    visible: parent===null?
                 false : uuid === parent.currentWindowUuid;

    // Alias to canvas dimensions
    property alias canvW: canvas.width;
    property alias canvH: canvas.height;


    // SIGNALS

    // Send message
    signal sendMsg(string str);
    signal broadcastFlagChanged(bool flag);
    signal subscriptionFlagChanged(bool flag);


    //  uuid
    property string uuid: ""

    //  name assigned to this AirBoard
    property string name: "Unnamed"

    //  Whether we are in broadcasting mode
    property bool broadcastEnabled: false
    onBroadcastEnabledChanged: {
        broadcastFlagChanged(broadcastEnabled);
    }

    //  Whether we are in subscription mode
    property bool subscriptionEnabled: false
    onSubscriptionEnabledChanged: {
        subscriptionFlagChanged(subscriptionEnabled);
    }

    // Received message string
    property string msgStr: ""
    property var msgList

    onMsgStrChanged: {
        defineMsgListIfNotDefined();
        msgList.push(msgStr);
    }

    // Currently selected color for drawing
    // Default is kept white
    property alias selectedColor: optionsOverlay.selectedColor;

    // Currently selected Stroke Size for drawing
    property alias selectedStrokeSize: optionsOverlay.selectedStrokeSize

    // Canvas background
    property string backgroundColor: whiteboardWrapper.color;

    MessageBox{
        id: msgBox;
        objectName: "MessageBoxObj"
    }

    //define msgList if not defined
    function defineMsgListIfNotDefined()
    {
        if(typeof msgList === "undefined")
        {
            msgList = [];
        }
    }

    // Url for the canvas image
    function canvasImgSrc()
    {
        var imgUrl = canvas.toDataURL("image/png");
        //console.log(imgUrl)
        return imgUrl;
    }

    onVisibleChanged: {
        if(visible)
        {
            console.log("Canvas:" + uuid + " is ready for drawing..");
            canvasTimer.start();
        }
        else
        {
            console.log("Canvas:" + uuid + " is closed..");
            canvasTimer.stop();
        }
    }

    // Function to exit Settings Window
    function exitSettingsWindow()
    {
        // For closing all settings menu, only thing we have to do is
        // to set the settingsMode flag OFF
        whiteboardWrapper.parent.settingsMode=false;
    }

    DevicePropertyValue { id: dpv; }

    // Canvas for drawing
    // It fills entire screen
    Canvas{
        id:canvas
        z: 2
        anchors.fill: parent

        property bool drawStart: true

        onPaint:{
            var ctx = canvas.getContext('2d');
            ctx.strokeStyle = selectedColor
            ctx.lineWidth = selectedStrokeSize;
            ctx.stroke();
        }

        function drawLine(p)
        {

            var msg;
            var ctx = canvas.getContext('2d');
            //ctx.beginPath();

            // console.log(drawStart, ": ", p.x, ", ", p.y);

            if(drawStart)
            {
                ctx.moveTo(p.x, p.y);
                drawStart=false;
            }
            else
            {
                ctx.lineTo(p.x, p.y);
            }
            requestPaint();
            //...
        }

        function beginDraw()
        {
            drawStart=true;
            var ctx = canvas.getContext('2d');
            ctx.beginPath();
        }
    }

    MouseArea{
        id: mouseArea
        anchors.fill: parent
        hoverEnabled: true

        property int currX: -1;
        property int currY: -1;

        // Reset pos
        onPressed: {
            currX = mouseX;
            currY = mouseY;
            canvas.beginDraw();
        }

        onReleased: {
            canvas.beginDraw();
        }

        onExited: {
            canvas.beginDraw();
        }

        // Update drawing when mouse pos changes
        onPositionChanged: {
            if(pressed)
            {
                currX = mouseX;
                currY = mouseY;
                canvasTimer.drawPending=true;
            }
            else
            {
                canvas.beginDraw();
            }
        }
    }

    // Get current mouse position and accordingly pass points to canvas for drawing
    function drawPieceOfLine()
    {
        if(mouseArea.currX < 0 ||
           mouseArea.currY < 0)
        {
            return;
        }

        // Point
        var p = {"x": mouseArea.currX, "y": mouseArea.currY};

        // If broadcast enabled, send draw message
        if(broadcastEnabled)
        {
            var msg;
            if(canvas.drawStart)
            {
                msg = msgBox.beginDrawMessageStr(optionsOverlay.selectedColor.toString().replace("#", "C"),
                                                 optionsOverlay.selectedStrokeSize/canvW,
                                                 0);
                sendMsg(msg);
            }

            msg = msgBox.drawMessageStr(p, canvW, canvH,  0);
            sendMsg(msg);
        }

        // Draw line
        canvas.drawLine(p);
    }

    // Parse incoming message and take actions
    function handleIncomingMsg(unprocessedMsg)
    {
        var tokenList = unprocessedMsg.split(":");
        if(tokenList.length > 2 && tokenList[0]==="B")
        {
            // start draw
            if(tokenList[1]==="S")
            {
                optionsOverlay.selectedColor = tokenList[2].replace("C", "#");
                optionsOverlay.selectedStrokeSize = parseFloat(tokenList[3])*canvW;
                if(optionsOverlay.selectedStrokeSize < 1.0)
                    optionsOverlay.selectedStrokeSize=1.0;
                canvas.beginDraw();
            }
            else if(tokenList[1]==="D")
            {
                // Point
                var p = {"x": parseFloat(tokenList[2])*canvW, "y": parseFloat(tokenList[3])*canvH};
                // Draw line
                canvas.drawLine(p);
            }
        }
    }

    Timer{
        id: canvasTimer
        property bool drawPending: false
        triggeredOnStart: true
        interval: 5
        repeat: true

        onTriggered: {

            //if subscriptionEnabled is true we handle messages
            if(subscriptionEnabled)
            {
                defineMsgListIfNotDefined();
                while(msgList.length > 0)
                {
                    var unprocessedMsg = msgList.shift();
                    // console.log(unprocessedMsg);
                    handleIncomingMsg(unprocessedMsg);
                }

                // we block user drawing when subscribed
                return;
            }

            if(drawPending)
            {
                drawPieceOfLine();
                drawPending=false;
            }
            else
            {
                if(!mouseArea.containsMouse)
                    canvas.beginDraw();
            }
        }
    }

    // Options panel
    WhiteBoardOptionsPanel
    {
        id: optionsOverlay
        visible: {
            // Visible when Currently Visible window is this
            // And settings mode is ON
            return whiteboardWrapper.parent !== null &&
                   uuid === whiteboardWrapper.parent.currentWindowUuid &&
                   whiteboardWrapper.parent.settingsMode;
        }
        z: 3
        anchors.fill: parent
    }
}
