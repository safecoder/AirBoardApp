
:: Visit target folder
cd %AIRBOARD_BUILD_TARGET%

:: Create a bin folder where we copy all required files
mkdir bin

::: Copy files into /bin folder
:::: First, Copy Application binaries
echo "Copying AirBoard binaries.."

IF EXIST "build" (
    copy build\release\AirBoardApp.exe bin\
) else (
    echo "AirBoard Build Directory not found. Build might have failed"
	set caught_error=1
	exit /b
)

:::: Second, Copy QT DLLs
echo "Copying QT DLLs.."

IF EXIST "%QT_PATH%" (
    copy %QT_PATH%\Qt5Core.dll bin\
	copy %QT_PATH%\icuin53.dll bin\
	copy %QT_PATH%\icuuc53.dll bin\
	copy %QT_PATH%\icudt53.dll bin\
	copy %QT_PATH%\Qt5Quick.dll bin\
	copy %QT_PATH%\Qt5Gui.dll bin\
	copy %QT_PATH%\Qt5Qml.dll bin\
	copy %QT_PATH%\Qt5Network.dll bin\
	copy %QT_PATH%\libgcc_s_dw2-1.dll bin\
	copy "%QT_PATH%\libstdc++-6.dll" bin\
	copy %QT_PATH%\libwinpthread-1.dll bin\
) else (
    echo "Qt Directory not found.."
	set caught_error=1
	exit /b
)

:: Go back to where we came from
cd %package_dir%
