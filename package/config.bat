::::::: CONFIGURATION TO SET UP REQUIRED PATHS

:: Current dir OR the package dir
set package_dir=%cd%

:: QT 5.4 Bin Directory Path
set QT_PATH=C:\Qt\Qt5.4.1\5.4\mingw491_32\bin

:: AirBoard Build Folder Path (Abs or Relative to curr dir)
set AIRBOARD_BUILD_TARGET=%cd%\..\..\Package_AB

:: AirBoard Source Folder Path (Abs or Relative to Build dir)
set AIRBOARD_SRC=%cd%\..\

:: NSIS Installation Directory
set NSIS_PATH=C:\Program Files (x86)\NSIS

:: Check if required paths exists
if not exist %QT_PATH% (
	echo "Error: Qt path doesn't exist !"
	set caught_error=1
	exit /b
)

if not exist %NSIS_PATH% (
	echo "Error: NSIS path doesn't exist !"
	set caught_error=1
	exit /b
)