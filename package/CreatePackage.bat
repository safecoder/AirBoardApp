set caught_error=0

:: Read App version
call read_version.bat

::: Get Required Paths
call config.bat

:: Build the source
call build.bat

::: Copy all required binary
call copyfiles.bat

:: Create installer
"%NSIS_PATH%\makensis.exe" "create_setup.nsi"
