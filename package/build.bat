
:: Setting current directory as AIRBOARD_APP_DIR directory
FOR /F "tokens=1" %%i in ('cd') do set AIRBOARD_APP_DIR=%%i

:: Set debug/release option
set OSSBUILD=%1%

set OSSARCH=x86

set MING32_PATH=C:\Qt\Qt5.4.1\Tools\mingw491_32\bin

if not exist %MING32_PATH% (
	echo "Error: Min32 path doesn't exist !"
	set caught_error=1
	exit /b
)

:: Add QT 5.4.0 bin to PATH
set PATH=%QT_PATH%;%MING32_PATH%;%PATH%

:: make visit build dir
mkdir %AIRBOARD_BUILD_TARGET%
cd %AIRBOARD_BUILD_TARGET%

:: Make bulid folder and generate build files.
mkdir build
cd build

:: Generate makefiles for Visual Studio 2013
qmake %AIRBOARD_SRC%\AirBoardApp.pro -r -spec "win32-g++" 

:: Compile code
mingw32-make -f Makefile.Release
mingw32-make clean

:: Go back to where we came from
cd %package_dir%