;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; Warning:
;		This is modified version of one of the NSIS examples.
;		Author is not responsible for whatever damage it might
;		cause to you by using or misusing this code.
;
;		Use it at YOUR OWN RISK.
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;Include helpful logic liberary
!include "LogicLib.nsh"

;Include Modern UI
!include "MUI2.nsh"

!define PRODUCT_NAME "CP Lab"
!define PRODUCT_VERSION "1.0"
!define SETUP_NAME               "CPLabSetup.exe"

OutFile ${SETUP_NAME}
Name "${PRODUCT_NAME} ${PRODUCT_VERSION}"

InstallDir "$PROGRAMFILES\CPLab"
InstallDirRegKey HKLM "Software\CP Lab" ""
ShowInstDetails show
ShowUnInstDetails show

SetCompressor /SOLID lzma
SetCompressorDictSize 12

  ;Request application privileges for Windows Vista
  RequestExecutionLevel user


Section "Dummy Section" SecDummy

  SetOutPath "$INSTDIR"

  ;Store installation folder
  WriteRegStr HKCU "Software\CP Lab" "" $INSTDIR

  ;Create uninstaller
  WriteUninstaller "$INSTDIR\Uninstall.exe"

SectionEnd


Section "Uninstall"

  ;ADD YOUR OWN FILES HERE...

  Delete "$INSTDIR\Uninstall.exe"

  RMDir "$INSTDIR"

  DeleteRegKey /ifempty HKCU "Software\Modern UI Test"

SectionEnd

!insertmacro MUI_PAGE_WELCOME
!insertmacro MUI_PAGE_LICENSE "${NSISDIR}\Docs\Modern UI\License.txt"
!insertmacro MUI_PAGE_COMPONENTS
!insertmacro MUI_PAGE_DIRECTORY
!insertmacro MUI_PAGE_INSTFILES
!insertmacro MUI_PAGE_FINISH

!insertmacro MUI_UNPAGE_WELCOME
!insertmacro MUI_UNPAGE_CONFIRM
!insertmacro MUI_UNPAGE_INSTFILES
!insertmacro MUI_UNPAGE_FINISH

;--------------------------------
;Languages

!insertmacro MUI_LANGUAGE "English"


;--------------------------------
;Descriptions

  ;Language strings
  LangString DESC_SecDummy ${LANG_ENGLISH} "A test section."

  ;Assign language strings to sections
  !insertmacro MUI_FUNCTION_DESCRIPTION_BEGIN
    !insertmacro MUI_DESCRIPTION_TEXT ${SecDummy} $(DESC_SecDummy)
  !insertmacro MUI_FUNCTION_DESCRIPTION_END

Function .OnInit
  StrCpy $0 "Welcome to my first setup wizard"
  push $0
  Call ShowWelcome
FunctionEnd

Function ShowWelcome
  pop $R0
  ${If} $R0 == ''
    StrCpy $R0 "Message from function"
  ${EndIf}

  MessageBox MB_OK $R0
FunctionEnd